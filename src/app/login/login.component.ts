import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
selector: 'login',
templateUrl: 'login.component.html',
styleUrls: ['login.component.css'],
})
export class LoginComponent implements OnInit {
    hide = true;
    constructor() { }

    ngOnInit() {
    }

    email = new FormControl('', [Validators.required, Validators.email]);

    getErrorMessage() {
        return this.email.hasError('required') ? 'You must enter a value' : this.email.hasError('email') ? 'Not a valid email' : '';
    }
}
