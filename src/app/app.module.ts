import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatCardModule, MatFormFieldModule, MatInputModule, MatGridListModule, MatTabsModule, MatSidenavModule, MatMenuModule} from '@angular/material';

@NgModule({
declarations: [
AppComponent
],
imports: [
BrowserModule,
BrowserAnimationsModule,
MatButtonModule,
MatCardModule,
MatFormFieldModule,
MatInputModule,
MatGridListModule,
MatCheckboxModule,
MatTabsModule,
MatSidenavModule, 
MatMenuModule, 
LoginModule
],
providers: [],
bootstrap: [AppComponent, LoginComponent]
})
export class AppModule { }
